package com.example.htmlparsing;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView sayilar;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new arkaPlan().execute();
    }


    class arkaPlan extends AsyncTask<Void, Integer, Void> {

        //Islem calistirilacak method
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            sayilar = findViewById(R.id.sayilar);
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.show();
        }

        //Arkaplan islemlerini gerceklestirelecek method
        @Override
        protected Void doInBackground(Void... voids) {

            for (int i = 0; i < 10; i++) {


                try {
                    publishProgress(i);
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }


            return null;
        }

        //bir islemin durumunun takip edilmesini saglayacak buton
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            Integer idegeri = values[0];
            Log.i("i degerleri : ", "" + idegeri);
            sayilar.setText(String.valueOf(idegeri));

        }

        //Arkaplan islemi sonrasi yapilacak olan islerin yapildigi method
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            sayilar.setText("Nihayet Bitti.");
            progressDialog.cancel();

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }
}


