package com.example.htmlparsing;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;

public class Main2Activity extends AppCompatActivity {
    private static final String url = "http://kurafloyka.com/htmlparsing.html";
    Document document;

    TextView title, header;
    Elements elements, srcElements, liElements;
    Bitmap bitmap;
    String imgUrl;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        new htmlGetir().execute();
    }


    public class htmlGetir extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                document = Jsoup.connect(url).get();

                //InputStream inputStream = new java.net.URL(imgUrl).openStream();
                //bitmap = BitmapFactory.decodeStream(inputStream);

                liElements = document.select("li");
//ul>li
                for (int i = 0; i < liElements.size(); i++) {

                    Log.i("elements", liElements.get(i).text());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //Log.i("source", document.title());
            title.setText(document.title());
            elements = document.select("h1");
            Log.i("H1 element", elements.text());
            srcElements = document.select("img[src]");
            imgUrl = srcElements.attr("src");
            Log.i("IMG", imgUrl);
            //imageView.setImageBitmap(bitmap);

            Picasso.with(getApplicationContext()).load(imgUrl).into(imageView);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            title = findViewById(R.id.title);
            header = findViewById(R.id.header);
            imageView = findViewById(R.id.image);
        }
    }
}
